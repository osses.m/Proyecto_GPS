package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class activity_display extends AppCompatActivity {
    DatabaseHelper miDB;
    ListView listac;
    Button menu3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        menu3 = findViewById(R.id.menu3); //REFERENCIA BOTON MENU

        menu3.setOnClickListener(new View.OnClickListener() {  //METODO VOLVER MENU
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_display.this, MainActivity.class);
                startActivity(intent);
            }
        });


        listac = (ListView)findViewById(R.id.ListView);
        miDB = new DatabaseHelper(this);

        ArrayList <String>Listados = new ArrayList<>();
        Cursor data = miDB.getListaContenidos();
        if (data.getCount() == 0){
            Toast.makeText(this,"NO HAY LISTA QUE MOSTRAR",Toast.LENGTH_LONG).show();
        }else{
            while(data.moveToNext()){
                Listados.add(data.getString(1));
                ListAdapter listAdapter = new ArrayAdapter<>(
                        this, android.R.layout.simple_list_item_1,Listados);
                listac.setAdapter(listAdapter);

            }
        }

    }
}
package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button menuupdate,menumaps,menudisplay,menusave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menusave = findViewById(R.id.menusave); // REFERENCIA BOTON GUARDAR
        menuupdate = findViewById(R.id.menuupdate);// REFERENCIA BOTON MODIFICAR
        menudisplay = findViewById(R.id.menudisplay);// REFERENCIA BOTON MOSTRAR
        menumaps = findViewById(R.id.menumaps); // REFERENCIA BOTON MAPS



        menusave.setOnClickListener(new View.OnClickListener() {  // METODO IR PAGINA SAVE
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, activity_save.class);
                startActivity(intent);
            }
        });

        menuupdate.setOnClickListener(new View.OnClickListener() {  // METODO IR PAGINA UPDATE
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, activity_update.class);
                startActivity(intent);
            }
        });

        menudisplay.setOnClickListener(new View.OnClickListener() {  // METODO IR PAGINA DISPLAY
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, activity_display.class);
                startActivity(intent);

            }
        });

        menumaps.setOnClickListener(new View.OnClickListener() {  // METODO INCIAR MAPS
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext() ,MapsActivity1.class);
                startActivity(intent);
            }
        });
        }
    }

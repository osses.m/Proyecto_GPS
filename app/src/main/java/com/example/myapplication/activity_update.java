package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class activity_update extends AppCompatActivity {

    Button menu2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        menu2 = findViewById(R.id.menu2); //REFERENCIA BOTON MENU

        menu2.setOnClickListener(new View.OnClickListener() { //METODO VOLVER MENU
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity_update.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}